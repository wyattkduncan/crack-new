#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

#include "md5.h"


const int PASS_LEN=50;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings


int cmpfunc(const void * key, const void * elem)
{
  // printf("%s\n", (char*)key);
   //printf("%s\n", (char*)elem);
    
   int ret = strncmp((char*)key, (char*)elem, 32);
   return ret;
    
}

int compare(const void *a, const void *b) {
    int ret = strncmp(a, b, 32);
    //printf("%d\n",ret);
    return ret;
}
// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5

    // Compare the two hashes

    // Free any malloc'd memory

    return 0;
}

// TODO
// Read in the hash file and return the array of strings.
char **read_hashes(char *filename)
{
    struct stat info;
    stat(filename, &info);
    int  file_length = info.st_size;
    
    char *chunk = malloc(file_length);
    
    FILE *f = fopen(filename, "r");
    if (!f) {
        perror("Error reading file");
        exit(1);
    }
    
    fread(chunk, 1, file_length, f);
    fclose(f);
    
    int lines = 0;
    for(int i = 0 ; i < file_length ; i++) {
        if (chunk[i] == '\n') {
            chunk[i] = '\0';
            lines++;
        }  
    }
    
    char **strings = malloc((lines + 1) * sizeof(char *));
    int string_idx = 0;
    
    for (int i = 0 ; i < file_length ; i += strlen(chunk + i) + 1) {
        strings[string_idx] = chunk + i;
        string_idx++;
    }
    strings[string_idx] = NULL;
    return strings;
}


// TODO
// Read in the dictionary file and return the data structure.
// Each entry should contain both the hash and the dictionary
// word.
char **read_dict(char *filename)
{
    char pass[PASS_LEN];
    char fixed[81]; //this will be my EXACT string length for sort and compare
    
    struct stat info;
    stat(filename, &info);
    
    FILE *f = fopen(filename, "r");
    if (!f) {
        perror("Error reading file");
        exit(1);
    }
    
    FILE *table = fopen("rainbow.txt", "w");
    if (!table) {
        perror("Error reading file");
        exit(1);
    }
    
    while(fscanf(f,"%s", pass) != EOF){
            sprintf(fixed, "%s:%s", md5(pass, strlen(pass)), pass);
            fprintf(table, "%-80s\n", fixed);
            
    }
    
    fclose(table);
    fclose(f);
    
    struct stat info2;
    stat("rainbow.txt", &info2);
    int file_length2 = info2.st_size;
    
    char *chunk2 = malloc(file_length2);
    
    FILE *g = fopen("rainbow.txt", "r");
    if (!g) {
        perror("Error reading file");
        exit(1);
    }
    
    
    
    
    fread(chunk2, 1, file_length2, g);
    fclose(g);
    
    int lines = 0;
    for(int i = 0 ; i < file_length2 ; i++) {
        if (chunk2[i] == '\n') {
            chunk2[i] = '\0';
            lines++;
        } 
        
    }
    
    char **strings = malloc((lines + 1) * sizeof(char *));
    int string_idx = 0;
    
    for (int i = 0 ; i < file_length2 ; i += strlen(chunk2 + i) + 1) {
        strings[string_idx] = chunk2 + i;
        string_idx++;
    }
    strings[string_idx] = NULL;
    return strings;
    
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // TODO: Read the hash file into an array of strings
    char **hashes = read_hashes(argv[1]);

    // TODO: Read the dictionary file into an array of strings
    char **dict = read_dict(argv[2]);
    
    
    
    // TODO: Sort the hashed dictionary using qsort.
    // You will need to provide a comparison function.
    
    int count =0;
    for (int i =1; dict[i] != NULL; i++){
        count++;
    }
    qsort(*dict, count, sizeof(char) * 81, compare);
   
    for (int i = 0 ; hashes[i] != NULL ; i++) {
        //printf("%s\n", dict[i]);
        int * w = bsearch(hashes[i], *dict, count, sizeof(char) * 81, cmpfunc);
        if (w != NULL) {
            printf("%s\n", dict[i]);
        }
        else {
            printf("%s not found\n", hashes[i]);
        }
    }
    printf("%d\n", count);
    // TODO
    // For each hash, search for it in the dictionary using
    // binary search.
    // If you find it, get the corresponding plaintext dictionary
    // entry. Print both the hash and word out.
    // Need only one loop. (Yay!)
    
    
    
}